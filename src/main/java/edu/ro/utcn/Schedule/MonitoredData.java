package edu.ro.utcn.Schedule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class MonitoredData {
    private String startTime;
    private String endTime;
    private String activity;

    public Date getDate(String string){
        Date date = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
        try{
            String[] strings = string.split("\\s\\s");
            date = simpleDateFormat.parse(strings[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public String getStartDay(){
        return startTime.split(" ")[0];
    }

    public LocalDateTime getTime(String string){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(string,formatter);
    }

    public MonitoredData(String startTime, String endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getActivity() {
        return activity;
    }

    @Override
    public String toString() {
        return "MonitoredData{" +
                "startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", activity='" + activity + '\'' +
                '}';
    }
}
