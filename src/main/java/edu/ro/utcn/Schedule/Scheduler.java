package edu.ro.utcn.Schedule;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Scheduler {
    private List<MonitoredData> schedule;

    public void initStream(String file) {
        try (Stream<String> stream = Files.lines(Paths.get(file))) {
            schedule = stream
                    .map(line -> {
                        String[] strings = line.split("\\s\\s");
                        return new MonitoredData(strings[0], strings[1], strings[2]);
                    })
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public long distinctDays() {
        return schedule.stream().map(m -> m.getDate(m.getStartTime())).distinct().count();
    }

    public void cntEachActivity() {
        Map<String, Long> map = schedule.stream().map(m -> m.getActivity()).collect(Collectors.groupingBy(
                Function.identity(), Collectors.counting()
        ));

        for (String a : map.keySet()) {
            System.out.println("Activity " + a + " == " + map.get(a));
        }
    }

    public void cntEachDay() {
        Map<String, Map<String, Long>> map = schedule.stream().collect(
                Collectors.groupingBy(MonitoredData::getStartDay,
                        Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())
                )
        );
        for (String day : map.keySet()) {
            System.out.println("Day " + day);
            Map<String, Long> cnt = map.get(day);
            for (String a : cnt.keySet()) {
                System.out.println("Activity " + a + " == " + cnt.get(a));
            }
            System.out.println();
        }
    }

    public void getDuration() {
        List<Long> durations = schedule.stream().map(m ->
                Duration.between(m.getTime(m.getStartTime()), m.getTime(m.getEndTime())).getSeconds() / 60)
                .collect(Collectors.toList());
        int line = 1;
        for (Long duration : durations) {
            System.out.println("Line: " + line++ + " Duration " + duration);
        }
    }

    public void entireDuration(){
        Map<String,Long> map = schedule.stream()
                .collect(Collectors.groupingBy(m->m.getActivity(),Collectors.summingLong(m->Duration.between(m.getTime(m.getStartTime()), m.getTime(m.getEndTime())).getSeconds() / 60)));
        for(String string : map.keySet()){
            System.out.println("Activity: " + string + " == " + map.get(string));
        }
    }

    public void filter(){
//        List<String> list = schedule.stream().filter(m->
//                (9/10)*schedule.stream().filter(m1->
//                        m1.getActivity() == m.getActivity()).count() == schedule.stream());
    }

    public void printSchedule() {
        for (MonitoredData data : schedule) {
            System.out.println(data);
        }
    }

    public Scheduler() {
        schedule = new ArrayList<>();
    }
}
