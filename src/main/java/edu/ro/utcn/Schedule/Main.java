package edu.ro.utcn.Schedule;

public class Main {
    private static final String file = "src/Activities.txt";
    public static void main(String[] args){
        Scheduler scheduler = new Scheduler();
        scheduler.initStream(file);
        System.out.println("=============Scheduler============");
        scheduler.printSchedule();
        System.out.println();
        System.out.println("Distinct Days: " + scheduler.distinctDays());
        System.out.println();
        scheduler.cntEachActivity();
        System.out.println();
        scheduler.cntEachDay();
        scheduler.getDuration();
        System.out.println();
        scheduler.entireDuration();
        System.out.println();
        scheduler.filter();
    }
}
